import simpy
import random
import os




class coffe_machine(object):
    def __init__(self, env):
        self.env = env
        self.run_prc=env.process(self.run(env))
        self.choice = ""
        self.func_finish=False
        self.sim_time=96

    def machine_state(self):
        water = random.randint(0, 1)
        milk = random.randint(0, 1)
        coffe = random.randint(0, 1)
        suggar = random.randint(0, 1)
        tank_full = ''

        if water == 0 and tank_full == '':
            while tank_full != 'F':
                print("\nBrak wody, uzupełnij zasobnik!")
                tank_full = input("Aby napełnic zbiornik wciśnij F: ").upper()
        else:
            print("\nZbiornik wody pełny, przechodzę dalej...")

        if milk == 0 and tank_full == '':
            while tank_full != 'F':
                print("\nBrak mleka, uzupełnij zasobnik!")
                tank_full = input("Aby napełnic zbiornik wciśnij F: ").upper()
        else:
            print("\nZbiornik mleka pełny, przechodzę dalej...")

        if coffe == 0 and tank_full == '':
            while tank_full != 'F':
                print("\nBrak kawy, uzupełnij zasobnik!")
                tank_full = input("Aby napełnic zbiornik wciśnij F: ").upper()
        else:
            print("\nZbiornik kawy pełny, przechodzę dalej...")

        if suggar == 0 and tank_full == '':
            while tank_full != 'F':
                print("\nBrak cukru, uzupełnij zasobnik!")
                tank_full = input("Aby napełnic zbiornik wciśnij F: ").upper()
        else:
            print("\nZbiornik cukru pełny, przechodzę dalej...")

    def coffe_choosing(self):

        x = ""
        coffe_menu = {
            0: "Kawa z mlekiem",
            1: "Kawa latte",
            2: "Esspresso",
            3: "Americano",
        }
        while x != "S":
            if x == "" or x == "G":
                self.choice = coffe_menu.get(0)
                print("\nWybrano: ", self.choice)
                x = input("Aby przejść dalej wciśnij 'S', aby zmienić wybór wciśnij 'D': ").upper()
            elif x == "D" or x == "GG":
                self.choice = coffe_menu.get(1)
                print("\nWybrano: ", self.choice)
                x = input("Aby przejść dalej wciśnij 'S', aby zmienić wybór wciśnij 'D' lub 'G': ").upper()
                if x == "D":
                    x = "DD"
            elif x == "DD" or x == "GGG":
                self.choice = coffe_menu.get(2)
                print("\nWybrano: ", self.choice)
                x = input("Aby przejść dalej wciśnij 'S', aby zmienić wybór wciśnij 'D' lub 'G': ").upper()
                if x == "D":
                    x = "DDD"
                elif x == "G":
                    x = "GG"
            elif x == "DDD":
                self.choice = coffe_menu.get(3)
                print("\nWybrano: ", self.choice)
                x = input("Aby przejść dalej wciśnij 'S', aby zmienić wybór wciśnij 'G': ").upper()
                if x == "D":
                    x = input("Ostatnia pozycja, wyciśnij 'G':").upper()
                    if x == "G":
                        x = "GGG"


        print("\nPrzygotowyanie kawy: ", self.choice)
        return self.choice

    def coff_comp(self):
        #zmienne pomocnicze
        x = ""
        c = ""
        v = ""
        q = ""
        comp_menu = {
            0: "Cukier",
            1: "Kawa",
            2: "Mleko",
        }
        suggar_pow = {
            0: "Cukier 0",
            1: "Cukier 1",
            2: "Cukier 2",
            3: "Cukier 3",
            4: "Cukier 4",
            5: "Cukier 5",
        }
        milk_pow = {
            0: "Mleko 0",
            1: "Mleko 1",
            2: "Mleko 2",
            3: "Mleko 3",
            4: "Mleko 4",
            5: "Mleko 5",
        }
        coff_pow = {
            0: "Kawa 0",
            1: "Kawa 1",
            2: "Kawa 2",
            3: "Kawa 3",
            4: "Kawa 4",
            5: "Kawa 5",
        }

        prompt = input("[Stan: Dobór składników]Czy chcesz modyfikować każdy składnik? "
                       "[T-Tak/N-nie,ustaw wartości domyślne]").upper()
        if prompt == "T":
            choice_coff = coff_pow.get(3)
            choice_sug = suggar_pow.get(3)
            choice_milk = milk_pow.get(3)
            while x != "Z":
                x = input(
                    "[Stan: Dobór składników] Który składnik chcesz zmodyfikować? [C-Cukier,M-Mleko,K-Kawa] "
                    "Jeżeli chcesz zaakcpetować wartości wprowadź Z: ").upper()

                if x == "C":
                    choice = comp_menu.get(0)
                    choice_sug = suggar_pow.get(0)
                    while c != "Z":
                        if c == "" or c == "M":
                            choice_sug = suggar_pow.get(0)
                            print("[Stan: Dobór cukru] Aktualna ilość", choice, ":", choice_sug)
                            c = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()

                        elif c == "W" or c == "MM":
                            choice_sug = suggar_pow.get(1)
                            print("[Stan: Dobór cukru] Aktualna ilość", choice, ":", choice_sug)
                            c = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if c == "W":
                                c = "WW"
                        elif c == "WW" or c == "MMM":
                            choice_sug = suggar_pow.get(2)
                            print("[Stan: Dobór cukru] Aktualna ilość", choice, ":", choice_sug)
                            c = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if c == "W":
                                c = "WWW"
                            elif c == "M":
                                c = "MM"
                        elif c == "WWW" or c == "MMMM":
                            choice_sug = suggar_pow.get(3)
                            print("[Stan: Dobór cukru] Aktualna ilość", choice, ":", choice_sug)
                            c = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if c == "W":
                                c = "WWWW"
                            elif c == "M":
                                c = "MMM"
                        elif c == "WWWW" or c == "MMMMM":
                            choice_sug = suggar_pow.get(4)
                            print("[Stan: Dobór cukru] Aktualna ilość", choice, ":", choice_sug)
                            c = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if c == "W":
                                c = "WWWWW"
                            elif c == "M":
                                c = "MMMM"
                        elif c == "WWWWW" or c == "MMMMMM":
                            choice_sug = suggar_pow.get(5)
                            print("[Stan: Dobór cukru] Aktualna ilość", choice, ":", choice_sug)
                            c = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if c == "W":
                                print("Maksymalna ilość cukru!")
                                c = input("[M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                                if c == "M":
                                    c = "MMMMM"
                    print("Wybrano: ", choice_sug)

                if x=="K":
                    choice = comp_menu.get(1)
                    choice_coff = coff_pow.get(0)
                    while v != "Z":
                        if v == "" or v == "M":
                            choice_coff = coff_pow.get(0)
                            print("[Stan: Dobór kawy]Aktualna ilość", choice, ":", choice_coff)
                            v = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()

                        elif v == "W" or v == "MM":
                            choice_coff = coff_pow.get(1)
                            print("[Stan: Dobór kawy] Aktualna ilość", choice, ":", choice_coff)
                            v = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if v == "W":
                                v = "WW"
                        elif v == "WW" or v == "MMM":
                            choice_coff = coff_pow.get(2)
                            print("[Stan: Dobór kawy] Aktualna ilość", choice, ":", choice_coff)
                            v = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if v == "W":
                                v = "WWW"
                            elif v == "M":
                                v = "MM"
                        elif v == "WWW" or v == "MMMM":
                            choice_coff = coff_pow.get(3)
                            print("[Stan: Dobór kawy] Aktualna ilość", choice, ":", choice_coff)
                            v = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if v == "W":
                                v = "WWWW"
                            elif v == "M":
                                v = "MMM"
                        elif v == "WWWW" or v == "MMMMM":
                            choice_coff = coff_pow.get(4)
                            print("[Stan: Dobór kawy] Aktualna ilość", choice, ":", choice_coff)
                            v = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if v == "W":
                                v = "WWWWW"
                            elif v == "M":
                                v = "MMMM"
                        elif v == "WWWWW" or v == "MMMMMM":
                            choice_coff = coff_pow.get(5)
                            print("[Stan: Dobór kawy] Aktualna ilość", choice, ":", choice_coff)
                            v = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if v == "W":
                                print("Maksymalna ilość kawy!")
                                v = input("[M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                                if v == "M":
                                    v = "MMMMM"
                    print("Wybrano: ", choice_coff)

                if x=="M":
                    choice = comp_menu.get(2)
                    choice_milk = milk_pow.get(0)
                    while q != "Z":
                        if q == "" or q == "M":
                            choice_milk = milk_pow.get(0)
                            print("[Stan: Dobór mleka] Aktualna ilość", choice, ":", choice_milk)
                            q = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()

                        elif q == "W" or q == "MM":
                            choice_milk = milk_pow.get(1)
                            print("[Stan: Dobór mleka] Aktualna ilość", choice, ":", choice_milk)
                            q = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if q == "W":
                                q = "WW"
                        elif q == "WW" or q == "MMM":
                            choice_milk = milk_pow.get(2)
                            print("[Stan: Dobór mleka] Aktualna ilość", choice, ":", choice_milk)
                            q = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if q == "W":
                                q = "WWW"
                            elif q == "M":
                                q = "MM"
                        elif q == "WWW" or q == "MMMM":
                            choice_milk = milk_pow.get(3)
                            print("[Stan: Dobór mleka] Aktualna ilość", choice, ":", choice_milk)
                            q = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if q == "W":
                                q = "WWWW"
                            elif q == "M":
                                q = "MMM"
                        elif q == "WWWW" or q == "MMMMM":
                            choice_milk = milk_pow.get(4)
                            print("[Stan: Dobór mleka] Aktualna ilość", choice, ":", choice_milk)
                            q = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if q == "W":
                                q = "WWWWW"
                            elif q == "M":
                                q = "MMMM"
                        elif q == "WWWWW" or q == "MMMMMM":
                            choice_milk = milk_pow.get(5)
                            print("[Stan: Dobór mleka] Aktualna ilość", choice, ":", choice_milk)
                            q = input("[W-dodaj więcej, M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                            if q == "W":
                                print("Maksymalna ilość mleka!")
                                q = input("[M-dodaj mniej] Aby zatwierdzić ilość wciśnij Z").upper()
                                if q == "M":
                                    q = "MMMMM"
                    print("Wybrano: ", choice_milk)

            if choice_milk != suggar_pow.get(3) and choice_coff != coff_pow.get(3) and choice_sug != milk_pow.get(3):
                        print("Zaakceptowano wartości:"
                        "\n", choice_sug,
                        "\n", choice_milk,
                        "\n", choice_coff)
            elif choice_sug != suggar_pow.get(3) and choice_coff != coff_pow.get(3):
                            milk = milk_pow.get(3)
                            print("Zaakceptowano wartości:"
                      "\n", choice_sug,
                      "\n", milk,
                      "\n", choice_coff)
            elif choice_sug != suggar_pow.get(3) and choice_milk != milk_pow.get(3):
                        coffe = coff_pow.get(3)
                        print("Zaakceptowano wartości:"
                      "\n", choice_sug,
                      "\n", choice_milk,
                      "\n", coffe)
            elif choice_coff != coff_pow.get(3) and choice_milk != milk_pow.get(3):
                        suggar = suggar_pow.get(3)
                        print("Zaakceptowano wartości:"
                      "\n", suggar,
                      "\n", choice_milk,
                      "\n", choice_coff)
            elif choice_coff != coff_pow.get(3):
                        suggar = suggar_pow.get(3)
                        milk = milk_pow.get(3)
                        print("Zaakceptowano wartości:"
                      "\n", suggar,
                      "\n", milk,
                      "\n", choice_coff)
            elif choice_milk != milk_pow.get(3):
                        suggar = suggar_pow.get(3)
                        coffe = coff_pow.get(3)
                        print("Zaakceptowano wartości:"
                      "\n", suggar,
                      "\n", choice_milk,
                      "\n", coffe)
            elif choice_sug != suggar_pow.get(3):
                        milk = milk_pow.get(3)
                        coffe = coff_pow.get(3)
                        print("Zaakceptowano wartości:"
                      "\n", choice_sug,
                      "\n", milk,
                      "\n", coffe)
            else:
                        milk = milk_pow.get(3)
                        suggar = suggar_pow.get(3)
                        coffe = coff_pow.get(3)
                        print("Zaakceptowano wartości:"
                      "\n", milk,
                      "\n", suggar,
                      "\n", coffe)
            print("Rozpoczynam przygotowywanie kawy")
            self.func_finish = True
        else:
            milk = milk_pow.get(3)
            suggar = suggar_pow.get(3)
            coffe = coff_pow.get(3)
            self.func_finish = True
            print("Rozpoczynam przygotowywanie kawy")
        return self.func_finish

    def make_milkcoffe(self, env):
        while True:
            print('\nWsypanie kawy do pojemnika w %d' % env.now)
            pour_duration = 5
            yield self.env.timeout(pour_duration)

            print('\nWsypanie cukru do pojemnika w %d' % env.now)
            suggar_duration = 5
            yield self.env.timeout(suggar_duration)

            print('\nZaparzenie kawy w pojemniku w %d' % env.now)
            scald_duration = 10
            yield self.env.timeout(scald_duration)

            print('\nNalanie kawy do kubka w %d' % env.now)
            fill_duration = 5
            yield self.env.timeout(fill_duration)

            print('\nDolanie mleka w %d' % env.now)
            pourmilk_duration = 5
            yield self.env.timeout(pourmilk_duration)

            print('\nKawa gotowa w %d' % env.now)
            ready_duration = 5
            yield self.env.timeout(ready_duration)

            print('\nKoniec pracy w %d' % env.now)
            endwork_duration = 5
            yield self.env.timeout(endwork_duration)

            print('\nWylączony w %d' % env.now)
            off_duration = 2
            yield self.env.timeout(off_duration)

    def make_latte(self,env):
        while True:
            print('\nWsypanie kawy do pojemnika w %d' % env.now)
            pour_duration = 5
            yield self.env.timeout(pour_duration)

            print('\nWsypanie cukru do pojemnika w %d' % env.now)
            suggar_duration = 5
            yield self.env.timeout(suggar_duration)

            print('\nZaparzenie kawy w pojemniku w %d' % env.now)
            scald_duration = 10
            yield self.env.timeout(scald_duration)

            print('\nNalanie kawy do kubka w %d' % env.now)
            fill_duration = 5
            yield self.env.timeout(fill_duration)

            print('\nDolanie mleka w %d' % env.now)
            pourmilk_duration = 5
            yield self.env.timeout(pourmilk_duration)

            print('\nSpienienie mleka w %d' % env.now)
            foam_duration = 5
            yield self.env.timeout(foam_duration)

            print('\nKawa gotowa w %d' % env.now)
            ready_duration = 5
            yield self.env.timeout(ready_duration)

            print('\nKoniec pracy w %d' % env.now)
            endwork_duration = 5
            yield self.env.timeout(endwork_duration)

            print('\nWylączony w %d' % env.now)
            off_duration = 2
            yield self.env.timeout(off_duration)

    def make_esspresso(self,env):
        while True:
            print('\nWsypanie kawy do pojemnika w %d' % env.now)
            pour_duration = 5
            yield self.env.timeout(pour_duration)

            print('\nWsypanie cukru do pojemnika w %d' % env.now)
            suggar_duration = 5
            yield self.env.timeout(suggar_duration)

            print('\nZaparzenie kawy w pojemniku w %d' % env.now)
            scald_duration = 10
            yield self.env.timeout(scald_duration)

            print('\nNalanie kawy do kubka w %d' % env.now)
            fill_duration = 5
            yield self.env.timeout(fill_duration)

            print('\nKawa gotowa w %d' % env.now)
            ready_duration = 5
            yield self.env.timeout(ready_duration)

            print('\nKoniec pracy w %d' % env.now)
            endwork_duration = 5
            yield self.env.timeout(endwork_duration)

            print('\nWylączony w %d' % env.now)
            off_duration = 2
            yield self.env.timeout(off_duration)

    def make_americano(self,env):

        while True:
            print('\nWsypanie kawy do pojemnika w %d' % env.now)
            pour_duration = 5
            yield self.env.timeout(pour_duration)

            print('\nWsypanie cukru do pojemnika w %d' % env.now)
            suggar_duration = 5
            yield self.env.timeout(suggar_duration)

            print('\nZaparzenie kawy w pojemniku w %d' % env.now)
            scald_duration = 10
            yield self.env.timeout(scald_duration)

            print('\nNalanie kawy do kubka w %d' % env.now)
            fill_duration = 5
            yield self.env.timeout(fill_duration)

            print('\nDolanie wody w %d' % env.now)
            pourwater_duration = 5
            yield self.env.timeout(pourwater_duration)

            print('\nKawa gotowa w %d' % env.now)
            ready_duration = 5
            yield self.env.timeout(ready_duration)

            print('\nKoniec pracy w %d' % env.now)
            endwork_duration = 5
            yield self.env.timeout(endwork_duration)

            print('\nWylączony w %d' % env.now)
            off_duration = 2
            yield self.env.timeout(off_duration)

    def run(self, env):
        while True:
            print('\nUruchamiam eksperes w %d' % env.now)
            starting_duration = 5
            yield self.env.timeout(starting_duration)

            print('\nSprawdzam stan maszyny w %d' % env.now)
            self.machine_state()
            check_duration = 20
            yield self.env.timeout(check_duration)

            print('\nWybór kawy w %d' % env.now)
            self.coffe_choosing()
            choosing_duration=30
            yield self.env.timeout(choosing_duration)

            prompt = input("[Stan: Dobór składników] Czy chcesz modyfikować ilość składników (tj. cukier, mleko)? "
                           "[T-Tak/N-Nie]: ").upper()
            if prompt == "T":
                self.coff_comp()
                if self.choice == "Kawa z mlekiem" and self.func_finish == True:
                    making = env.process(self.make_milkcoffe(env))
                    making_duration = env.timeout(42)
                    yield making & making_duration
                elif self.choice =="Kawa latte" and self.func_finish == True:
                    making = env.process(self.make_latte(env))
                    making_duration = env.timeout(47)
                    yield making & making_duration
                elif self.choice =="Esspresso" and self.func_finish == True:
                    making = env.process(self.make_esspresso(env))
                    making_duration = env.timeout(37)
                    yield making & making_duration
                elif self.choice=="Americano" and self.func_finish == True:
                    making = env.process(self.make_americano(env))
                    making_duration = env.timeout(42)
                    yield making & making_duration

            else:
                print("Przechodze dalej")
                if self.choice == "Kawa z mlekiem":
                    self.sim_time
                    making = env.process(self.make_milkcoffe(env))
                    making_duration = env.timeout(42)
                    yield making & making_duration
                elif self.choice =="Kawa latte":
                    making = env.process(self.make_latte(env))
                    making_duration = env.timeout(47)
                    self.sim_time = 101
                    yield making & making_duration
                elif self.choice =="Esspresso":
                    making = env.process(self.make_esspresso(env))
                    making_duration = env.timeout(37)
                    self.sim_time = 91
                    yield making & making_duration
                elif self.choice=="Americano":
                    making = env.process(self.make_americano(env))
                    making_duration = env.timeout(42)
                    self.sim_time = 96
                    yield making & making_duration


env = simpy.Environment()
cm = coffe_machine(env)
env.run(until=cm.sim_time)
print(cm.sim_time)

os.system("PAUSE")